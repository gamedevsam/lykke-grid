/*
 * @Author: Samuel Batista 
 * @Date: 2017-01-28 17:28:17 
 * @Last Modified by: Samuel Batista
 * @Last Modified time: 2017-01-29 22:05:15
 */
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import GridContainer from './grid/GridContainer';
import { Navbar, Nav, NavItem, NavDropdown, MenuItem } from 'react-bootstrap';

// Nice tsx guide: http://www.typescriptlang.org/docs/handbook/jsx.html
export default class App extends React.Component<{}, {}> {
	public render() {
		return (
			<div className="container-fluid">
				{/* Render Bootstrap NavBar */}
				<Navbar>
					<Navbar.Header>
						<Navbar.Brand>Lykke-Grid</Navbar.Brand>
					</Navbar.Header>
					<Nav>
						<NavDropdown eventKey={3} title="Grid Styles" id="basic-nav-dropdown">
							<MenuItem eventKey={3.1}>Small</MenuItem>
							<MenuItem eventKey={3.2}>Regular</MenuItem>
							<MenuItem eventKey={3.3}>Large</MenuItem>
						</NavDropdown>
					</Nav>
				</Navbar>
				{/* Render Grid */}
				<GridContainer/>
			</div>
		);
	}
}

ReactDOM.render(<App/>, document.getElementById('root'));