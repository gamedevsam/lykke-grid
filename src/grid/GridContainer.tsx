/*
 * @Author: Samuel Batista 
 * @Date: 2017-01-28 17:27:54 
 * @Last Modified by: Samuel Batista
 * @Last Modified time: 2017-01-31 14:54:43
 */
import Grid from './Grid';
import * as React from 'react';
import { IGridData } from '../@types/lykke-grid';

export default class GridContainer extends React.PureComponent<any, IGridData> {
	constructor(props) {
		super(props);
		var defaultWidth = 100;
		// Initial state, this would normally come from a remote server
		this.state = {
			columns: [
				{
					key: 'name',
					label: 'Name',
					resizable: true,
					width: defaultWidth
				},
				{
					key: 'chart',
					label: '+/-',
					chart: true,
					resizable: false,
					width: defaultWidth
				},
				{
					key: 'sell',
					label: 'Sell',
					colored: true,
					resizable: true,
					width: defaultWidth
				},
				{
					key: 'buy',
					label: 'Buy',
					colored: true,
					resizable: true,
					width: defaultWidth
				},
				{
					key: 'change',
					label: 'Change',
					colored: true,
					resizable: true,
					width: defaultWidth
				},
				{
					key: 'changePercent',
					label: 'in %',
					percent: true,
					resizable: true,
					width: defaultWidth
				},
				{
					key: 'update',
					label: 'Update',
					resizable: true,
					width: defaultWidth
				},
				{
					key: 'delete',
					label: 'Del',
					resizable: false,
					width: defaultWidth,
					iconButton: true
				}
			],
			rows: [
			{
				name: 'AUDCAD',
				chart: 1,
				sell: 0.98318,
				buy: 0.98318,
				change: 0.0002,
				changePercent: 0.065,
				update: '14:35:35',
				delete: 'icon icon--close'
			},
			{
				name: 'AUDPLN',
				chart: -1,
				sell: -0.98318,
				buy: -0.98318,
				change: -0.0002,
				changePercent: -0.065,
				update: '14:35:35',
				delete: 'icon icon--close'
			},
			{
				name: 'AUDNOK',
				chart: 1,
				sell: 0.98318,
				buy: 0.98318,
				change: 0.0002,
				changePercent: 0.065,
				update: '14:35:35',
				delete: 'icon icon--close'
			},
			{
				name: 'AUDSEC',
				chart: -1,
				sell: -0.98318,
				buy: 0.98318,
				change: -0.0002,
				changePercent: -0.065,
				update: '14:35:35',
				delete: 'icon icon--close'
			},
			{
				name: 'AUDJPY',
				chart: 1,
				sell: 0.98318,
				buy: 0.98318,
				change: 0.0002,
				changePercent: 0.065,
				update: '14:35:35',
				delete: 'icon icon--close'
			},
			{
				name: 'AUDUSD',
				chart: 1,
				sell: 0.98318,
				buy: 0.98318,
				change: 0.0002,
				changePercent: 0.065,
				update: '14:35:35',
				delete: 'icon icon--close'
			},
			{
				name: 'USDEUR',
				chart: 1,
				sell: 0.98318,
				buy: 0.98318,
				change: 0.0002,
				changePercent: 0.065,
				update: '14:35:35',
				delete: 'icon icon--close'
			}]
		}
		
		// Randomize values to simulate live data updates
		var self:any = this;
		let shuffleData = function(firstTime) {
			var len = self.state.rows.length;
			var copy = self.state.rows.slice()
			for(let i = 0; i < len; ++i) {
				let row = copy[i];
				let positive = Math.random() > 0.5;
				row.chart = positive ? 1 : -1;
				row.sell = ((positive ? Math.random() : -Math.random()) * 100).toFixed(2);
				row.buy = ((positive ? Math.random() : -Math.random()) * 100).toFixed(2);
				row.change = (positive ? Math.random() : -Math.random()).toFixed(3);
				row.changePercent = ((positive ? Math.random() : -Math.random()) * 10).toFixed(3);
				row.update = new Date().toLocaleTimeString();
			}
			if(!firstTime) self.setState({ rows: copy });
		}
		setInterval(shuffleData, 3000);
		shuffleData(true);
	}
	render() {
		return (
			<Grid columns={this.state.columns} rows={this.state.rows} />
		);
	 }
};