/*
 * @Author: Samuel Batista 
 * @Date: 2017-01-28 17:28:42 
 * @Last Modified by: Samuel Batista
 * @Last Modified time: 2017-01-31 14:22:34
 */
import GridCell from './GridCell';
import * as React from 'react';
import * as FDT from 'fixed-data-table';
import { IGridProps, IGridState } from '../@types/lykke-grid';

export default class Grid extends React.PureComponent<IGridProps, IGridState> {
	constructor(props) {
		super(props);
		this.state = { columnWidths: new Array(this.props.columns.length) }
		for(let i = 0; i < this.props.columns.length; ++i) {
			this.state.columnWidths[i] = this.props.columns[i].width;
		}
		this.onResizeHandleDoubleClick = this.onResizeHandleDoubleClick.bind(this);
		this.onColumnResizeEndCallback = this.onColumnResizeEndCallback.bind(this);
	}
	public onResizeHandleDoubleClick(columnKey) {
		for(var i = 0; i < this.props.columns.length; ++i) {
			if(this.props.columns[i].key == columnKey) {
				let copy = this.state.columnWidths.slice();
				copy[i] = this.props.columns[i].width;
				this.setState({ columnWidths: copy });
				return;
			}
		}
	}
	public onColumnResizeEndCallback(newColumnWidth, columnKey) {
		for(var i = 0; i < this.props.columns.length; ++i) {
			if(this.props.columns[i].key == columnKey) {
				let minWidth = this.props.columns[i].width;
				let copy = this.state.columnWidths.slice();
				copy[i] = newColumnWidth > minWidth ? newColumnWidth : minWidth;
				this.setState({ columnWidths: copy });
				return;
			}
		}
	}
	public render() {
		return (
			<FDT.Table
				width={screen.width - 45}
				height={screen.height}
				headerHeight={40}
				rowHeight={50}
				rowsCount={this.props.rows.length}
				onResizeHandleDoubleClick={this.onResizeHandleDoubleClick}
				onColumnResizeEndCallback={this.onColumnResizeEndCallback}
				isLoading={false}>
					{this.props.columns.map(function(col:any, i:number) {
						return (
							<FDT.Column
								key={i}
								fixed={i == 0}
								columnKey={col.key}
								isResizable={col.resizable}
								width={this.state.columnWidths[i]}
								minWidth={col.width}
								header={<FDT.Cell>{col.label}</FDT.Cell>}
								cell={<GridCell column={col} rows={this.props.rows} rowIndex={-1}/>} // rowIndex is automatically set by fixed-data-table
							/>
						)
					}, this)
				}
			</FDT.Table>
		)
	}
}