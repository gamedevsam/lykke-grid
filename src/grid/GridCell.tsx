import * as React from 'react';
import * as FDT from 'fixed-data-table';
import { IGridCellProps } from '../@types/lykke-grid';

export default class GridCell extends React.PureComponent<IGridCellProps, any> {
	render() {
		let rows = this.props.rows;
		let index = this.props.rowIndex;
		let column = this.props.column;
		let value = rows[index][column.key];

		if(column.chart) return <ChartCell value={value}/>
		if(column.percent) return <PercentCell value={value}/>
		if(column.colored) return <ColoredCell value={value}/>
		if(column.iconButton) return <IconButtonCell value={value}/>

		// fallback, render pure text
		return <TextCell value={value}/>
	}
}

const TextCell = ({value}) => (
	<FDT.Cell>
		<span className="status_label">{value}</span>
	</FDT.Cell>
)
const IconButtonCell = ({value}) => (
	<FDT.Cell>
		<button type="button" className="btn btn--icon">
			<i className={value}/>
		</button>
	</FDT.Cell>
)
const ChartCell = ({value}) => (
	<FDT.Cell>
		{function(value:number) {
			if(value > 0) return <i className="icon icon--up text--green"/>
			else if(value < 0) return <i className="icon icon--down text--red"/>
		}(value)}
	</FDT.Cell>
)
const ColoredCell = ({value}) => (
	<FDT.Cell>
		{colorizeText(value)}
	</FDT.Cell>
)
const PercentCell = ({value}) => (
	<FDT.Cell>
		{colorizeText(value, '%')}
	</FDT.Cell>
)
function colorizeText(value:number, postFix?:string) {
	var result = postFix ? value + postFix : value;
	if(value > 0) return <span className='text--green'>{result}</span>
	else if(value < 0) return <span className='text--red'>{result}</span>
	else return <span>{result}</span>
}
