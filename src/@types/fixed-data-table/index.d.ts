// Additional type definitions for fixed-data-table
// Project: https://github.com/gamedevsam/fixed-data-table.git
// Definitions by: Samuel Batista <https://github.com/gamedevsam>
// Definitions: https://github.com/DefinitelyTyped/DefinitelyTyped
// TypeScript Version: 2.1

/// <reference types="react"/>

import * as React from 'react';
import { Table } from 'fixed-data-table';

declare module 'fixed-data-table' {
	export interface TableProps extends React.Props<Table> {
		/**
			* Callback that is called when resizer has been released
			* and column needs to be updated.
			*
			* Required if the isResizable property is true on any
			* column.
			*/
		onResizeHandleDoubleClick?: (columnKey: string) => void;

		/**
			* Whether a column is currently being resized.
			*/
		isLoading?: boolean;
	}
}
