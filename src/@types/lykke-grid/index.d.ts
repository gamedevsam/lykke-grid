// Type definitions for lykke-grid
// Project: https://github.com/gamedevsam/fixed-data-table.git
// Definitions by: Samuel Batista <https://github.com/gamedevsam>
// Definitions: https://github.com/DefinitelyTyped/DefinitelyTyped
// TypeScript Version: 2.1

import * as React from 'react';

export = LykkeGrid;
export as namespace LykkeGrid;

declare namespace LykkeGrid {
	export interface IRow {
		name:string,
		chart:number,
		sell:number,
		buy:number,
		change:number,
		changePercent:number,
		update:string,
		delete:string
	}
	export interface IColumn {
		key:string,
		label:string,
		width:number,
		resizable:boolean,
		chart?:boolean,
		colored?:boolean,
		percent?:boolean,
		iconButton?:boolean
	}
	export interface IGridData {
		rows:Array<IRow>,
		columns:Array<IColumn>
	}
	interface IGridState {
		columnWidths:Array<number>
	}
	export interface IGridProps extends IGridData, React.Props<any> {}
	export interface IGridCellProps extends React.Props<any> {
		rowIndex:number,
		rows:Array<IRow>,
		column:IColumn
	}
}
