var path = require('path');
var webpack = require('webpack');
var failPlugin = require('webpack-fail-plugin');

module.exports = {
	entry: './src/App.tsx',
	entry: [
		'react-hot-loader', // activate HMR for React
		'webpack-dev-server/client?http://localhost:3000', // bundle the client for webpack-dev-server and connect to the provided endpoint
		'webpack/hot/only-dev-server', // bundle the client for hot reloading
		'./src/App.tsx' // the entry point of our app
	],
	output: {
		path: path.join(__dirname, 'dist'),
		filename: 'bundle.js',
		publicPath: '/static/'
	},
	resolve: {
		extensions: ['.css', '.js', '.ts', '.tsx']
	},
	module: {
		rules: [
			// load typescript
			{ test: /\.ts$/, use: 'ts-loader' },
			{ test: /\.tsx$/, use: 'ts-loader' },
			// load source maps
			{ test: /\.map$/, use: "source-map-loader" },
			// load css styles
			{ test: /\.css$/, use: "css-loader" },
			// load fonts and other raw assets
			{ test: /\.eot$/, use: 'raw-loader' },
			{ test: /\.woff$/, use: 'raw-loader' },
			{ test: /\.ttf$/, use: 'raw-loader' },
			{ test: /\.svg$/, use: 'raw-loader' },
		]
	},
	plugins: [
		failPlugin,
		new webpack.HotModuleReplacementPlugin(), // enable HMR globally
		new webpack.NamedModulesPlugin(), // prints more readable module names in the browser console on HMR updates
	]
}